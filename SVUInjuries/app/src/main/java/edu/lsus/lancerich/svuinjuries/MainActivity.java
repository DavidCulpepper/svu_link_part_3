package edu.lsus.lancerich.svuinjuries;

import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    Activity context = MainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LinearLayout injuryList = (LinearLayout) findViewById(R.id.injuryList);
        final EditText injuries = (EditText) findViewById(R.id.injuries);
        Button addInjury = (Button) findViewById(R.id.addInjury);
        CheckBox medicalHelp = (CheckBox) findViewById(R.id.medicalHelp);
        CheckBox medicalForm = (CheckBox) findViewById(R.id.medicalForm);
        EditText medicalFacility = (EditText) findViewById(R.id.medicalFacility);
        FloatingActionButton finished = (FloatingActionButton) findViewById(R.id.finished);

        addInjury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                injuryList.addView(context.getLayoutInflater().inflate(R.layout.injury_list, injuryList, false));
            }
        });
    }
}
